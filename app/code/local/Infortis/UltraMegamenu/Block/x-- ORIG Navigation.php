<?php
/**
 * Infortis UltraMegamenu - category navigation menu
 */

class Infortis_UltraMegamenu_Block_Navigation extends Mage_Catalog_Block_Navigation
{
	//NEW:
	protected $_isAccordion = FALSE;
	protected $_isWide = FALSE;
	protected $_lev1_WideClass = '';
	protected $_tplProcessor = NULL;
	//end:NEW
	
	/**
     * Render category to html
     *
     * @param Mage_Catalog_Model_Category $category
     * @param int Nesting level number
     * @param boolean Whether ot not this item is last, affects list item class
     * @param boolean Whether ot not this item is first, affects list item class
     * @param boolean Whether ot not this item is outermost, affects list item class
     * @param string Extra class of outermost list items
     * @param string If specified wraps children list in div with this class
     * @param boolean Whether ot not to add on* attributes to list item
     * @return string
     */
    protected function _renderCategoryMenuItemHtml($category, $level = 0, $isLast = false, $isFirst = false,
        $isOutermost = false, $outermostItemClass = '', $childrenWrapClass = '', $noEventAttributes = false)
    {
        if (!$category->getIsActive()) {
            return '';
        }
        $html = array();

        // get all children
        if (Mage::helper('catalog/category_flat')->isEnabled()) {
            $children = (array)$category->getChildrenNodes();
            $childrenCount = count($children);
        } else {
            $children = $category->getChildren();
            $childrenCount = $children->count();
        }
        $hasChildren = ($children && $childrenCount);

        // select active children
        $activeChildren = array();
        foreach ($children as $child) {
            if ($child->getIsActive()) {
                $activeChildren[] = $child;
            }
        }
        $activeChildrenCount = count($activeChildren);
        $hasActiveChildren = ($activeChildrenCount > 0);
		
		//WHAT: category's custom blocks (child blocks) --------------------------------------------------
		//NEW:
		$helper = Mage::helper('ultramegamenu');
		$cat = Mage::getModel('catalog/category')->load($category->getId());
		
		//Get category blocks only in wide menu.
		$getChildBlocks = FALSE;
		if ($this->_isWide)
		{
			//By default get and show category blocks only if category has children (sub-categories).
			//This behavior can be overridden by admin settings.
			$getChildBlocks = $hasActiveChildren;
			if ($helper->getCfg('widemenu/show_if_no_children'))
			{
				$getChildBlocks = true;
			}
		}
		
		/////////////////////////////////////////////////////////////////////////////////////////////
		//Render category blocks of the current top-level category.
		$htmlBeforeChildren = array();
		$htmlAfterChildren = array();
		$hasChildBlocks = false;
		$totalCols = 0;
		if ($level == 0 && $this->_isAccordion == FALSE && $getChildBlocks)
		{
			$tmpBlockRight = $this->_getCatBlock($cat, 'umm_cat_block_right');
			
			/*
			 * Get settings
			 */
			$MAX_COLS = 6;
			if ($proportions = $cat->getData('umm_cat_block_proportions'))
			{
				$proportions = explode("/", $proportions);
				$childrenCols = $proportions[0];
				$rightCols = $proportions[1];
			}
			else
			{
				$childrenCols = 4;
				$rightCols = 2;
			}
			$totalCols = $childrenCols + $rightCols;
			
			/*
			 * Constraints:
			 */
			if (empty($tmpBlockRight)) //If block right is empty
			{
				$rightCols = 0;
				$childrenGridClass = 'grid12-12'; //CSS grid class for block with sub-categories
			}
			elseif (!$hasActiveChildren) //If no sub-categories
			{
				$childrenCols = 0;
				$rightGridClass = 'grid12-12';
			}
			else
			{
				//Evaluate CSS grid classes for blocks
				$unitsPerCol = 12 / $totalCols;
				$childrenGridClass = 'grid12-' . ($childrenCols * $unitsPerCol);
				$rightGridClass = 'grid12-' . ($rightCols * $unitsPerCol);
			}
			//Recalculate the sum of columns
			$totalCols = $childrenCols + $rightCols;
			
			/*
			 * Check if there are any blocks in this category and get those blocks
			 */
			$tmpHtml = '';
			//Top
			if ($tmpHtml = $this->_getCatBlock($cat, 'umm_cat_block_top'))
			{
				$htmlBeforeChildren[] = '<div class="nav-block nav-block-top grid-full std">';
				$htmlBeforeChildren[] = $tmpHtml;
				$htmlBeforeChildren[] = '</div>';
			}
			//Children
			if ($hasActiveChildren)
			{
				$childrenColumnsClasses = 'itemgrid itemgrid-'. $childrenCols .'col';
				$htmlBeforeChildren[] = '<div class="nav-block nav-block-center '. $childrenGridClass .' '. $childrenColumnsClasses .'">';
				//...Here between these elements the list of cateogry's children (sub-categories) will be inserted...
				$htmlAfterChildren[] = '</div>';
			}
			//Right
			if ($tmpBlockRight)
			{
				$htmlAfterChildren[] = '<div class="nav-block nav-block-right std '. $rightGridClass .'">';
				$htmlAfterChildren[] = $tmpBlockRight;
				$htmlAfterChildren[] = '</div>';
			}
			//Bottom
			if ($tmpHtml = $this->_getCatBlock($cat, 'umm_cat_block_bottom'))
			{
				$htmlAfterChildren[] = '<div class="nav-block nav-block-bottom grid-full std">';
				$htmlAfterChildren[] = $tmpHtml;
				$htmlAfterChildren[] = '</div>';
			}
			
			if (!empty($htmlBeforeChildren) || !empty($htmlAfterChildren))
				$hasChildBlocks = true;
		}
		/////////////////////////////////////////////////////////////////////////////////////////////
		//end:NEW

        // prepare list item html classes
        $classes = array();
        $classes[] = 'level' . $level;
        $classes[] = 'nav-' . $this->_getItemPosition($level);
        if ($this->isCategoryActive($category)) {
            $classes[] = 'active';
        }
        $linkClass = '';
        if ($isOutermost && $outermostItemClass) {
            $classes[] = $outermostItemClass;
            $linkClass = ' class="'.$outermostItemClass.'"';
        }
        if ($isFirst) {
            $classes[] = 'first';
        }
        if ($isLast) {
            $classes[] = 'last';
        }
		
		//WHAT: menu item classes --------------------------------------------------
		
		//NEW:
		//Add parent class if category has children or category blocks
		if ($hasActiveChildren || $hasChildBlocks) {
            $classes[] = 'parent';
        }
		//Add wide menu item classes (if menu is not an accordion):
		//Second level:
		if ($level == 1 && $this->_isAccordion == FALSE && $this->_isWide) {
			$classes[] = 'item';
		}
		//end:NEW
		
        // prepare list item attributes
        $attributes = array();
        if (count($classes) > 0) {
            $attributes['class'] = implode(' ', $classes);
        }
        if ($hasActiveChildren && !$noEventAttributes) {
             $attributes['onmouseover'] = 'toggleMenu(this,1)';
             $attributes['onmouseout'] = 'toggleMenu(this,0)';
        }

        // assemble list item with attributes
        $htmlLi = '<li';
        foreach ($attributes as $attrName => $attrValue) {
            $htmlLi .= ' ' . $attrName . '="' . str_replace('"', '\"', $attrValue) . '"';
        }
        $htmlLi .= '>';
        $html[] = $htmlLi;
		
		//WHAT: menu item: above the link --------------------------------------------------
		
		/////////////////////////////////////////////////////////////////////////////////////////////
		//NEW:
		//Insert category block (top) of the current 2nd-level category. Above the link.
		if ($level == 1 && $this->_isAccordion == FALSE && $this->_isWide)
		{
			if ($tmpHtml = $this->_getCatBlock($cat, 'umm_cat_block_top'))
			{
				$html[] = '<div class="nav-block nav-block-level1-top std">';
				$html[] = $tmpHtml;
				$html[] = '</div>';
			}
		}
		/////////////////////////////////////////////////////////////////////////////////////////////
		
		//Insert category label
		$catLabel = $this->_getCategoryLabelHtml($cat, $level);
		//end:NEW

        $html[] = '<a href="' . $this->getCategoryUrl($category) . '"' . $linkClass . '>';
        $html[] = '<span>' . $this->escapeHtml($category->getName()) . $catLabel . '</span>'; //NEW: insert category label
		$html[] = '</a>';

        // render children
        $htmlChildren = '';

        $j = 0;
        foreach ($activeChildren as $child) {
            $htmlChildren .= $this->_renderCategoryMenuItemHtml(
                $child,
                ($level + 1),
                ($j == $activeChildrenCount - 1),
                ($j == 0),
                false,
                $outermostItemClass,
                $childrenWrapClass,
                $noEventAttributes
            );
            $j++;
        }
		
		//WHAT: category's children --------------------------------------------------
		
		//NEW:
		//In wide menu wrapp the first drop-down box (2nd-level categories) inside a div with 'level0-wrapper' class.
		//Add a special class with total number of columns in the drop-down box.
		if ($level == 0 && $this->_isAccordion == FALSE && $this->_isWide)
		{
			$childrenWrapClass = 'level0-wrapper dropdown-' . $totalCols . 'col';
		}
		//end:NEW
		
        if (!empty($htmlChildren) || $hasChildBlocks) { //NEW: added condition with $hasChildBlocks
			
			//NEW: add opener for the menu item if the menu is accordion.
			if ($this->_isAccordion == TRUE)
				$html[] = '<span class="opener">&nbsp;</span>';
			//end:NEW
			
            if ($childrenWrapClass) {
                $html[] = '<div class="' . $childrenWrapClass . '">';
            }
			$html[] = implode("", $htmlBeforeChildren); //NEW:
			
			if (!empty($htmlChildren)) //NEW: wrapped child sub-categories with another condition
			{
				$html[] = '<ul class="level' . $level . '">';
				$html[] = $htmlChildren;
				$html[] = '</ul>';
			}
			
			$html[] = implode("", $htmlAfterChildren); //NEW:
            if ($childrenWrapClass) {
                $html[] = '</div>';
            }
        }
		
		//WHAT: menu item: after children --------------------------------------------------
		
		/////////////////////////////////////////////////////////////////////////////////////////////
		//NEW:
		//Insert category block (bottom) of the current 2nd-level category. Below the link and children.
		if ($level == 1 && $this->_isAccordion == FALSE && $this->_isWide)
		{
			if ($tmpHtml = $this->_getCatBlock($cat, 'umm_cat_block_bottom'))
			{
				$html[] = '<div class="nav-block nav-block-level1-bottom std">';
				$html[] = $tmpHtml;
				$html[] = '</div>';
			}
		}
		/////////////////////////////////////////////////////////////////////////////////////////////
		//end:NEW

        $html[] = '</li>';

        $html = implode("\n", $html);
        return $html;
    }
	
	/**
     * Render categories menu in HTML
     *
	 * @param bool Add opener if menu is used as accordion.
     * @param int Level number for list item class to start from
     * @param string Extra class of outermost list items
     * @param string If specified wraps children list in div with this class
     * @return string
     */
    public function renderCategoriesMenuHtml($isAccordion = FALSE, $level = 0, $outermostItemClass = '', $childrenWrapClass = '')
    {
		//NEW:
		//Save additional attributes
		$this->_isAccordion = $isAccordion;
		$this->_isWide = Mage::helper('ultramegamenu')->getCfg('mainmenu/wide_menu');
		//end:NEW
		
        $activeCategories = array();
        foreach ($this->getStoreCategories() as $child) {
            if ($child->getIsActive()) {
                $activeCategories[] = $child;
            }
        }
        $activeCategoriesCount = count($activeCategories);
        $hasActiveCategoriesCount = ($activeCategoriesCount > 0);

        if (!$hasActiveCategoriesCount) {
            return '';
        }

        $html = '';
        $j = 0;
        foreach ($activeCategories as $category) {
            $html .= $this->_renderCategoryMenuItemHtml(
                $category,
                $level,
                ($j == $activeCategoriesCount - 1),
                ($j == 0),
                true,
                $outermostItemClass,
                $childrenWrapClass,
                true
            );
            $j++;
        }

        return $html;
    }
	
	/**
     * NEW:
	 * Get category block (attribute) content.
     *
	 * @param Mage_Catalog_Model_Category
     * @param string $attrId ID of the attribute
     * @return string
     */
    protected function _getCatBlock($cat, $attrId)
    {
		if (!$this->_tplProcessor)
		{
			$this->_tplProcessor = Mage::helper('cms')->getBlockTemplateProcessor();
		}
		
		return $this->_tplProcessor->filter( trim($cat->getData($attrId)) );
	}
	
	/**
     * NEW:
	 * Get category label HTML
     *
	 * @param Mage_Catalog_Model_Category
     * @return string
     */
    protected function _getCategoryLabelHtml($cat, $level)
    {
		$catLabelKey = $cat->getData('umm_cat_label');
		
		if ($catLabelKey)
		{
			$catLabelValue = trim(Mage::helper('ultramegamenu')->getCfg('category_labels/' . $catLabelKey));
			if ($catLabelValue)
			{
				if ($level == 0)
				{
					return ' <span class="cat-label cat-label-'. $catLabelKey .' pin-bottom">' . $catLabelValue . '</span>';
				}
				else
				{
					return ' <span class="cat-label cat-label-'. $catLabelKey .'">' . $catLabelValue . '</span>';
				}
			}
		}
		
		return '';
	}
}
